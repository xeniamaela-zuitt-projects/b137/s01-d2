package com.zuitt.s01d2;

public class DataTypesAndVariables {

    public static void main(String[] args) {

        //<dataType><identifier> = <value>;

        //variable declaration with initialization
        int firstNumber = 0;

        //variable declaration without initialization
        int secondNumber;
        secondNumber = 23; //initialization after declaration

        //concatenation
        System.out.println("The value of the first number: " + firstNumber);
        System.out.println("The value of the second number: " + secondNumber);

        //mini-activity
        //create a variable to contain an age value
        int age;
        age = 18;
        //create a variable to contain a human weight
        double weight;
        weight = 60.57;
        //create a variable to contain a human gender
        char gender;
        gender = 'M';


        //create a variable to contain a civil status is it is single
        boolean isSingle = true;

        System.out.println("Age is: " + age);
        System.out.println("Weight is: " + weight);
        System.out.println("Gender is: " + gender);
        System.out.println("Is single: " + isSingle);

        //non-primitives:

        //Create a string variable for a name
        String name = "Xenia Mae";
        String concatName = "Xenia" + "Mae" + "Abano";

        System.out.println("name is: " + name);
        System.out.println("concatName is: " + concatName);


    }
}
